from wines.wine import Wines, WinesSVC
from wines.prplot import PRPlot
from wines.stats import Stats
from wines.dataloader import DataLoader
from wines.utils import resource_filepath


def prepare_data():
    loader = DataLoader().load_data()
    win = Wines()
    win_svc = WinesSVC()
    stats_knn = Stats("KNeighborsClassifier")
    stats_svc = Stats("OneVsRestClassifier")
    prplot = PRPlot()
    return win, win_svc, loader, stats_knn, stats_svc, prplot


def train(model_filepath, X_train, y_train, win):
    win.train(X_train, y_train)
    win.serialize(model_filepath)


def predict(model_filepath, X_test):
    win = Wines.deserialize(model_filepath)
    return win.predict(X_test)


def main():
    model_filepath = resource_filepath("model.pkl")
    win, win_svc, loader, stats_knn, stats_svc, prplot = prepare_data()

    try:
        train(model_filepath, loader.X_train, loader.y_train, win)
        y_pred = predict(model_filepath, loader.X_test)
    except NameError:
        print("Data has not been loaded! Use load_data() function before training!")

    try:
        stats_knn.get_stats(loader.X_test, loader.y_test, y_pred)
    except NameError:
        print("You can't get stats until you train the data!")

    try:
        train(model_filepath, loader.X_train, loader.y_train, win_svc)
        y_pred_svc = predict(model_filepath, loader.X_test)
        stats_svc.get_stats(loader.X_test, loader.y_test, y_pred_svc)
        prplot.get_plot(loader.y_test, y_pred_svc)
    except AttributeError or UnboundLocalError:
        print("You have follow the correct order - train_ovr(), predict_ovr() and after that you can get stats "
              "or PR plot.")


if __name__ == "__main__":
    main()
