from matplotlib import pyplot as plt
from sklearn.metrics import precision_recall_curve, average_precision_score
import numpy as np


class PRPlot:
    @staticmethod
    def get_plot(y_test, y_pred):
        classes = [0, 1, 2]
        y_pred_svc = np.array([[0] * i + [1] + [0] * (len(classes) - i - 1) for i in y_pred]).astype(int)
        y_test_svc = np.array([[0] * i + [1] + [0] * (len(classes) - i - 1) for i in y_test]).astype(int)

        plt.figure(figsize=(12, 8))
        for i in classes:
            precision, recall, _ = precision_recall_curve(y_test_svc[:, i], y_pred_svc[:, i])
            average_precision = average_precision_score(y_test_svc[:, i], y_pred_svc[:, i])

            plt.plot(recall, precision, lw=2, alpha=.5,
                     label=f"Class {i} (average_precision = {average_precision:.2f})")
        plt.xlabel("Recall")
        plt.ylabel("Precision")
        plt.title("Precision recall plot for the OneVsRestClassifier")
        plt.legend(loc="lower left")
        plt.show()