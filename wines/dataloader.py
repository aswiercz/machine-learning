from sklearn.datasets import load_wine
from sklearn.model_selection import train_test_split


class DataLoader:
    def __init__(self):
        self.wines = load_wine()
        self.X = self.wines.data
        self.y = self.wines.target
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.test_size = None

    def load_data(self, test_size=0.5):
        self.test_size = test_size
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y,
                                                                                test_size=self.test_size,
                                                                                random_state=42)
        print(f"Data loaded!")
        print(f"Length of the loaded data: X - {len(self.X)}, y - {len(self.y)}")
        return self

