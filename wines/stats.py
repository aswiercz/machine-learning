from sklearn.metrics import classification_report
from sklearn.decomposition import PCA


class Stats:
    def __init__(self, name):
        self.name = name

    def get_stats(self, X_test, y_test, y_predictions):
        X_test_transformed = PCA(n_components=7).fit_transform(X_test)
        print(f"Test set for the {self.name}:\n{classification_report(y_test, y_predictions)}")
        score = sum(y_predictions == y_test) / len(X_test_transformed)
        print(f"The mean accuracy of the {self.name}: {score}")
        return score



