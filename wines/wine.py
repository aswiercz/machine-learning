from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.multiclass import OneVsRestClassifier
from sklearn.exceptions import NotFittedError
from sklearn import svm
from pickle import dump, load


class Wines:
    def __init__(self):
        self.pca = PCA(n_components=7)
        self.knn = KNeighborsClassifier(n_neighbors=3)
        self.X_test_transformed = None
        self.X_train_transformed = None
        self.y_pred = None

    def predict(self, X_test):
        try:
            self.X_test_transformed = self.pca.fit_transform(X_test)
            y_pred = self.knn.predict(self.X_test_transformed)
            return y_pred
        except NotFittedError:
            print("You have to train data before prediction!")
            quit()

    def train(self, X_train, y_train):
        self.X_train_transformed = self.pca.fit_transform(X_train)
        self.knn.fit(self.X_train_transformed, y_train)

    def serialize(self, model_filepath):
        with open(model_filepath, "wb") as f:
            dump(self, f)

    @staticmethod
    def deserialize(model_filepath):
        with open(model_filepath, "rb") as f:
            return load(f)


class WinesSVC(Wines):
    def __init__(self):
        self.ovr = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True, random_state=42))
        self.pca = PCA(n_components=7)
        self.X_test_transformed = None
        self.X_train_transformed = None
        self.y_pred = None

    def predict(self, X_test_svc):
        self.X_test_transformed = self.pca.fit_transform(X_test_svc)
        y_pred = self.ovr.predict(self.X_test_transformed)
        return y_pred

    def train(self, X_train, y_train):
        self.X_train_transformed = self.pca.fit_transform(X_train)
        self.ovr.fit(self.X_train_transformed, y_train)



