import os
from setuptools import setup, find_packages


def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        return f.read()


setup(
    name="wine",
    version="1.0",
    description="Script for recognition producer of the wine",
    author="Aleksandra Świercz",
    author_email="aleks.swiercz@gmail.com",
    url="",
    license="Apache 2.0",
    packages=find_packages(),
    install_requires=read("requirements.txt").splitlines(),
    long_description=read("README.md"),
    python_requires=">=3.6",
)
