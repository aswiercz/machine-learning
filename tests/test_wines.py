from wines.wine import Wines, WinesSVC
from wines.stats import Stats
from wines.dataloader import DataLoader
from pytest import fixture
import numpy
from wines.utils import resource_filepath, remove_file


@fixture
def win():
    return Wines()


@fixture
def win_svc():
    return WinesSVC()


@fixture
def load():
    return DataLoader().load_data()


@fixture(scope="session")
def model_filepath():
    model_filepath = resource_filepath("model.pkl")
    yield model_filepath
    remove_file(model_filepath)


def test_load():
    loader = DataLoader().load_data()
    test_size = loader.test_size

    assert len(loader.X_test) == test_size * 178
    assert len(loader.y_test) == test_size * 178
    assert loader.X_train is not None
    assert loader.y_train is not None


def test_wines(win, load):
    loader = load
    win.train(loader.X_train, loader.y_train)
    y_pred = win.predict(loader.X_test)

    assert isinstance(y_pred, numpy.ndarray)


def test_wines_svc(win_svc, load):
    loader = load
    win_svc.train(loader.X_train, loader.y_train)
    y_pred = win_svc.predict(loader.X_test)

    assert isinstance(y_pred, numpy.ndarray)


def test_serialization(win, model_filepath, load):
    loader = load
    win.train(loader.X_train, loader.y_train)
    win.serialize(model_filepath)
    win_deserialized = Wines.deserialize(model_filepath)

    assert win_deserialized.X_test_transformed == win.X_test_transformed


def test_serialization_svc(win_svc, model_filepath, load):
    loader = load
    win_svc.train(loader.X_train, loader.y_train)
    win_svc.serialize(model_filepath)
    win_svc_deserialized = Wines.deserialize(model_filepath)

    assert win_svc_deserialized.X_test_transformed == win_svc.X_test_transformed


def test_stats(load, win):
    loader = load
    stats = Stats("KNeighborsClassifier")
    win.train(loader.X_train, loader.y_train)
    y_pred = win.predict(loader.X_test)
    stat = stats.get_stats(loader.X_test, loader.y_test, y_pred)

    assert isinstance(stat, float)



