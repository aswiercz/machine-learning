# "Wines" script

## Description of the dataset
This is a copy of UCI ML Wine recognition datasets. https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data

The data is the results of a chemical analysis of wines grown in the same region in Italy by three different cultivators. 
There are thirteen different measurements taken for different constituents found in the three types of wine.

Original Owners:

Forina, M. et al, PARVUS - An Extendible Package for Data Exploration, Classification and Correlation. 
Institute of Pharmaceutical and Food Analysis and Technologies, Via Brigata Salerno, 16147 Genoa, Italy.

Citation:

Lichman, M. (2013). UCI Machine Learning Repository [https://archive.ics.uci.edu/ml]. 
Irvine, CA: University of California, School of Information and Computer Science.

## Description of the script
"wine.py" has class Wines and WinesSVC. Wines class has methods predict() and train() for KNeighborsClassifier 
as well as serialize() and deserialize(). 
WinesSVC class  inherits from the class WINES and also has methods train() and predict() but for OneVsRestClassifier.

"dataloader.py" has DataLoader with load_data() method.

"stats.py" has Stats class with get_stats() method.
While creating an instance you have to define the name of classificator you used.

"prplot.py" has class PRPlot for plotting Precision-Recall metric to evaluate OVR classifier output quality.
This one has static method - it's not necessary to create an instance.

First of all, you should load the data. After that you can train data and get prediction. 

The last thing is the possibility of obtaining prediction statistics and P-R curves.

You can run calculations using wines/__main__ script. 

You can run tests using pytest. 